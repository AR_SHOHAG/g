#include<stdio.h>

int fact(long long int n)
{
   long long int count=0;
   while(n>1){
    n = n/2;
    ++count;
   }
   return count;
}

int main()
{
    long long int T, temp, n, i;
    scanf("%lld", &T);

    for(i=1; i<=T; ++i){
        scanf("%lld", &n);

        if(n%2 == 1)
            printf("Chicken First!\n");
        else{
            temp = fact(n);
            if(temp%2 == 1)
                printf("Egg First!\n");
            else
                printf("Chicken First!\n");
        }
    }
    return 0;
}
